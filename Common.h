#ifndef COMMON_COMMON_H
#define COMMON_COMMON_H

#include <stdexcept>
#include <cstdarg>
#include <cstdio>
#if defined(__linux__) || defined(__APPLE__)
    #include <sys/time.h>
#elif defined(_WIN32)
    #define NOMINMAX
    #include <windows.h>
#endif

#define UNUSED(expr) (void)(expr);
#define UNUSED2(param1, param2) UNUSED(param1) UNUSED(param2)
#define UNUSED3(param1, param2, param3) UNUSED(param1) UNUSED2(param2, param3)
#define UNUSED4(param1, param2, param3, param4) UNUSED(param1) UNUSED3(param2, param3, param4)
#define UNUSED5(param1, param2, param3, param4, param5) UNUSED(param1) UNUSED4(param2, param3, param4, param5)
#define UNUSED6(param1, param2, param3, param4, param5, param6) UNUSED(param1) UNUSED5(param2, param3, param4, param5, param6)

/** Static assertion. */
#if defined(__GNUC__)
    #define CMPTO_STATIC_ASSERT_UNUSED __attribute__((unused))
#else
    #define CMPTO_STATIC_ASSERT_UNUSED
#endif
#define CMPTO_STATIC_ASSERT(expression) \
    CMPTO_STATIC_ASSERT_UNUSED static char CMPTO_JOIN(__cmpto_static_assertion, __LINE__)[(expression) ? 1 : -1]

namespace cmpto {
namespace common {

class Common
{
public:
    /** Throw std::exception containing given formatted string. */
    static inline const char * throwException(const char * format, ...)
    {
        char buffer[4096];

        va_list args;
        va_start(args, format);
    #if defined(_WIN32)
        vsprintf_s(buffer, 4096, format, args);
    #else
        vsprintf(buffer, format, args);
    #endif
        va_end(args);

        throw std::runtime_error(buffer);
    }

    /** @return current time in seconds */
    static inline double getTime()
    {
    #if defined(_WIN32)
        static double frequency = 0.0;
        static LARGE_INTEGER frequencyAsInt;
        LARGE_INTEGER timer;
        if (frequency == 0.0) {
            if (!QueryPerformanceFrequency(&frequencyAsInt)) {
                return -1.0;
            }
            else {
                frequency = (double)frequencyAsInt.QuadPart;
            }
        }
        QueryPerformanceCounter(&timer);
        return (double) timer.QuadPart / frequency;
    #elif defined(__linux__) || defined(__APPLE__)
        struct timeval tv;
        gettimeofday(&tv, 0);
        return (double) tv.tv_sec + (double) tv.tv_usec * 0.000001;
    #else
        #error Implement for current platform
    #endif
    }
};

} // namespace common
} // namespace cmpto

#endif // COMMON_COMMON_H
