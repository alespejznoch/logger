#include <locale>
#include <vector>
#include <string>
#include <sstream>

/**
 * @brief trim spaces and tabs from string
 * @param str
 * @return trimmed string
 */
inline std::string trim(const std::string& str)
{
	std::size_t first = str.find_first_not_of(" \t");
	if (std::string::npos == first)
	{
		return "";
	}
	size_t last = str.find_last_not_of(" \t");
	return str.substr(first, (last - first + 1));
}

/**
 * @brief SplitAndTrim
 * @param str
 * @param delimiter
 * @return array with two strings
 */
inline std::array<std::string,2> splitAndTrim(const std::string &str, const char &delimiter) {
	std::array<std::string,2> arr;
	size_t pos = str.find(delimiter);
	if (std::string::npos == pos) {
		return arr;
	}
	
	arr[0] = trim(str.substr(0,pos));
	arr[1] = trim(str.substr(pos+1,str.size()-pos));
	return arr;
}

/**
 * @brief split
 * @param s
 * @param delimeter
 * @return array with separated tokens by @param delimeter
 */
inline std::vector<std::string> split(const std::string &s, char delimeter)
{
    std::vector<std::string> elems;

    std::stringstream ss;
    ss.str(s);
    std::string item;

    while (std::getline(ss, item, delimeter)) {
        elems.push_back(item);
    }

	return elems;
}

/**
 * @brief is_number
 * @param s
 * @return true if value in s is a positive number
 */
inline bool isNumber(const std::string& s)
{
	std::locale loc;
	std::string::const_iterator it = s.begin();
	while (it != s.end() && std::isdigit(*it, loc))
	{
		++it;
	}
	return !s.empty() && it == s.end();
}

/**
 * @brief is_float
 * @param s
 * @return true if value in s is a float number
 */
inline bool isPositiveFloat(const std::string& s)
{
	std::locale loc;
	std::string::const_iterator it = s.begin();
	if (*it == '-')
	{
		return false;
	}
	int detectedPoint = 0;
	while (it != s.end())
	{
		if (std::isdigit(*it,loc))
		{
			++it;
			continue;
		}
		if (*it=='.')
		{
			++detectedPoint;
		}
		if (detectedPoint>1)
		{
			break;
		}
		++it;
	}
	return !s.empty() && it == s.end();
}
