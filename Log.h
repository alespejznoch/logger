#ifndef LOG_H
#define LOG_H

#include <string>
#include <vector>
#include <iostream>
#include <mutex>
#include <ctime>
#include <chrono>
#include <iomanip>
#include <unistd.h>
#include <sstream>
#include <fstream>
#include <map>
#include <sys/stat.h>
#include <fcntl.h>

//#include "Common.h"
#include "LogMagic.h"

namespace logger {

/// @brief check Log::spam(...)
#define writeLog(...) Log::getInstance().spam(__VA_ARGS__, __FILE__,__LINE__)

///////////////////////////////////////////////////////////////////////////////////////////
// EXAMPLE OF USE
// LOG report:
// writeLog(Header, log_category, log_level) << ... ;
// writeLog("ERROR opening file:", LogCat::GENERIC, 1) << file_name << "does not exist";
//
// DEBUG report:
// Log::debug("Variable X set");
///////////////////////////////////////////////////////////////////////////////////////////



// color constants for linux console
#define RESET					"\033[0m"
#define BOLD					"\033[1m"

#define BLACK					"\033[30m"						/* Black */
#define RED						"\033[31m"						/* Red */
#define GREEN					"\033[32m"						/* Green */
#define YELLOW					"\033[33m"						/* Yellow */
#define BLUE					"\033[34m"						/* Blue */
#define MAGENTA					"\033[35m"						/* Magenta */
#define CYAN					"\033[36m"						/* Cyan */
#define WHITE					"\033[37m"						/* White */

#define BOLDBLACK				"\033[1m\033[30m"				/* Bold Black */
#define BOLDRED					"\033[1m\033[31m"				/* Bold Red */
#define BOLDGREEN				"\033[1m\033[32m"				/* Bold Green */
#define BOLDYELLOW				"\033[1m\033[33m"				/* Bold Yellow */
#define BOLDBLUE				"\033[1m\033[34m"				/* Bold Blue */
#define BOLDMAGENTA				"\033[1m\033[35m"				/* Bold Magenta */
#define BOLDCYAN				"\033[1m\033[36m"				/* Bold Cyan */
#define BOLDWHITE				"\033[1m\033[37m"				/* Bold White */

// position in code
#define POS(x,y) x << ':' << BOLD << std::left << std::setw(3) << y << std::right << RESET << ' '
#define FPOS(x,y) x << ':' << y << ' '


static std::chrono::system_clock::time_point global_tp = std::chrono::system_clock::now();

/**
 * @brief getStartTS
 * @return timestamp in milliseconds
 */
inline unsigned long long getStartTS()
{
	using namespace std::chrono;

	system_clock::time_point tp = global_tp;
	auto duration = tp.time_since_epoch();
	
//	time_t t = system_clock::to_time_t(tp);
//	struct tm * now = localtime(&t);

//	std::stringstream ss;
//	ss << std::setfill('0');
	
//	ss		<< std::setw(2) << now->tm_hour << ':'
//			<< std::setw(2) << now->tm_min << '-';
	
//	ss		<< std::setw(2) << now->tm_mday << ':'
//			<< std::setw(2) << (now->tm_mon + 1) << ':'
//			<< (now->tm_year + 1900);
	
	return (unsigned long long)duration_cast<milliseconds>(duration).count();
}


/**
 * @brief writeDateTime
 * @param date Setting date to true will add date before time
 */
inline std::string writeDateTime(bool date)
{
	using namespace std::chrono;
	
	system_clock::time_point tp = system_clock::now();
	auto duration = tp.time_since_epoch();
	auto millis = duration_cast<milliseconds>(duration);
	millis -= duration_cast<seconds>(duration);
	
	time_t t = system_clock::to_time_t(tp);
	struct tm * now = localtime(& t);
	
	std::stringstream ss;
	
	// set formating
	ss << std::setfill('0');
	
	if (date)
	{
		ss	<< std::setw(2) << now->tm_mday << '.'
			<< std::setw(2) << (now->tm_mon + 1) << '.'
			<< (now->tm_year + 1900) << ' ';
	}
	
	ss		<< std::setw(2) << now->tm_hour << ':'
			<< std::setw(2) << now->tm_min << ':'
			<< std::setw(2) << now->tm_sec << '.'
			<< std::setw(3) << millis.count() << ' ';
	
	
	// reset formating
	ss << std::setfill(' ');
	
	return ss.str();
}


template<typename T>
std::ostream& operator<< (std::ostream& out, const std::vector<T>& v) {
	out << "[";
	size_t last = v.size() - 1;
	for(size_t i = 0; i < v.size(); ++i) {
		out << v[i];
		if (i != last)
			out << ", ";
	}
	out << "]";
	return out;
}


/**
 * @brief The Logger class
 */
class Logger
{

friend class Log; // for locking mutex

public:
	Logger(bool _console_ok, bool _file_ok, LogCat _cat, const std::string & file, int line, int level, const std::string & message)
		: console_ok(_console_ok), file_ok(_file_ok), cat(_cat)
	{
		if (not doesPrint())
			return;

		mtx.lock();

		std::string content = getLogName(cat);

		if (console_ok)
		{
			lockf(STDOUT_FILENO,F_LOCK,0);

			std::cerr << BOLD << writeDateTime(0);
			std::string level_color;

			switch (level)
			{
				case 0:
					level_color = RED;
					break;
				case 1:
					level_color = YELLOW;
					break;
				case 2:
					level_color = GREEN;
					break;
				case 3:
					level_color = CYAN;
					break;
				default:
					level_color = WHITE;
			}

			std::cerr << BOLD << level_color << std::left << std::setw(22) << content << std::right << RESET
				<< level_color << POS(file,line) << RESET << BOLD << "     " << message << RESET;
		}

		if (file_ok)
		{
			std::array<char, 64> buffer;
			buffer.fill(0);
			time_t rawtime;
			time(&rawtime);
			const auto timeinfo = localtime(&rawtime);
			strftime(buffer.data(), sizeof(buffer), "%Y-%m-%d_%H", timeinfo);
			std::string timeStr(buffer.data());
			char name[16];
			pthread_getname_np(pthread_self(), name, 16);

			filestream.open("logs/"+timeStr+"_"+ getLogName(cat) + "_" + name + "_" + std::to_string(getpid()) + ".log", std::ofstream::out | std::ofstream::app);

			switch (level)
			{
				case 0:
					content.append(":E");
					break;
				case 1:
					content.append(":W");
					break;
				case 2:
					content.append(":I");
					break;
				case 3:
					content.append(":i");
					break;
				default:
					content.append(":");
					content.append(std::to_string(level));
			}

			filestream << writeDateTime(0)
				<< std::left << std::setw(20) << content << std::right
				<< FPOS(file,line)
				<< "     " << message;
		}
	}

	Logger(const Logger &) = delete;
	Logger(Logger && o) {
		console_ok = o.console_ok;
		file_ok = o.file_ok;
		o.console_ok = false;
		o.file_ok = false;
		cat = o.cat;
		filestream = std::move(o.filestream);
	}
	
	~Logger()
	{
		if (console_ok) {
			std::cerr << std::endl; // flushes
			lockf(STDOUT_FILENO,F_ULOCK,0);
		}

		if (file_ok) {
			filestream << std::endl; // flushes
			filestream.close();
		}

		if (doesPrint())
			Logger::mtx.unlock();
	}

	Logger & operator=(const Logger &) = delete;
	Logger & operator=(Logger && o) = delete;
	
	bool doesPrint() const
	{
		return file_ok or console_ok;
	}

	template <typename T>
	Logger & operator<<(const T &item)
	{
		if (console_ok)
		{
			std::cerr << " " << item;
		}
		
		if (file_ok)
		{
			filestream << " " << item;
		}
		
		return *this;
	}

	Logger & operator<<(const bool &item)
	{
		if (console_ok)
			std::cerr << (item ? " true" : " false");
		
		if (file_ok)
			filestream << (item ? " true" : " false");
		
		return *this;
	}

private:
	static std::mutex mtx;
	bool console_ok;
	bool file_ok;
	std::fstream filestream;
	LogCat cat;
};















/**
 * @brief The Log class
 */
class Log
{
private:
	Log() {
		struct stat sb;
		
		// check for the existence of the directory logs
		if (stat("logs", &sb) != 0 || !S_ISDIR(sb.st_mode))
		{
			std::cerr << BOLDMAGENTA << "LOG WARNING: directory logs is missing. Creating a new one..." << RESET << std::endl;
			system("mkdir logs");
		}
	}
	
public:
	
	/**
	 * @brief getInstance
	 * @return 
	 */
	static Log& getInstance()
	{
		static Log spammer;
		return spammer;
	}
	
	Log(Log const&) = delete;
	void operator=(Log const&) = delete;
	
	/**
	 * @brief spam
	 * @param message
	 * @param cat
	 * @param level
	 * @param file
	 * @param line
	 * @return an object that is used to redefine operator <<
	 */
	Logger spam(std::string message, const LogCat &cat, unsigned int level, std::string file, int line)
	{
		std::pair<unsigned int, unsigned int> l = getLogLevel(cat);
		bool console_ok = false;
		bool file_ok = false;

		if (log_to_console && level<=l.first)
		{
			console_ok = true;
		}

		if (log_to_file && level<=l.second)
		{
			file_ok = true;
		}

		try {
			file = file.substr(remove_from_path);
		}
		catch (const std::out_of_range& oor) {
			std::cerr << "Out of Range error: " << oor.what() << '\n'
				<< "This possibly means, that function:' Log::getInstance().setRemoveFromPath(int pos); ' \n"
				"has bad position of input 'pos' variable. \n"
				"You can repair it by: Log::getInstance().setRemoveFromPath(1);\n"
				"This exception was cached in " << __FILE__
				<< " file at: " << __LINE__ << " row." << std::endl;
			throw;
		}
		
		return Logger(console_ok, file_ok, cat, file, line, level, message);
	}
	
	/**
	 * @brief debug
	 * @param msg
	 */
	static void debug(const std::string &msg = "")
	{
		if (!dev_mode)
		{
			return;
		}
		std::lock_guard<std::mutex> lck(Logger::mtx);
		lockf(STDOUT_FILENO,F_LOCK,0);
		std::cerr << BOLDBLUE <<"DEBUG:" << RESET << BLUE << " " << msg << RESET << std::endl;
		lockf(STDOUT_FILENO,F_ULOCK,0);
	}
	
	void setRemoveFromPath(size_t value)
	{
		remove_from_path = value;
	}
	
	void setLogToFile(bool value)
	{
		log_to_file = value;
	}
	
	void setLogToConsole(bool value)
	{
		log_to_console = value;
	}
	
	static bool dev_mode;
	

private:

	bool log_to_console                     = true;
	bool log_to_file                        = true;
	
	size_t remove_from_path = 0;
};

enum loglvl{
	ERROR          = 0,
	WARNING        = 1,
	INFO           = 2,
	DETAILED_INFO  = 3,
	LAST_LVL
};

enum devLoglvl{
	DEV_LVL_1 = loglvl::LAST_LVL,
	DEV_LVL_2
};

} // namespace logger

#endif
