
// The one and ONLY declaration of Logger categories
/////////////////////////////////////////////////////////////////////////////////////////////
BEGIN_ENUM(LogCat)
{
	TOP
	DECL_ENUM_ELEMENT(GENERIC)
	DECL_ENUM_ELEMENT(GREEN_MODULE)
	DECL_ENUM_ELEMENT(RED_MODULE)
	DECL_ENUM_ELEMENT(SMALL_PLUGIN)
	BOTTOM
}
END_ENUM
