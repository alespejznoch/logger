#ifndef LOG_MAGIC_H
#define LOG_MAGIC_H

#include <map>
#include <string>

namespace logger {

///// The class enums are gererated here
/////////////////////////////////////////////////////////////////////////////////////////////
#define DECL_ENUM_ELEMENT( element ) element,
#define TOP
#define BOTTOM
#define BEGIN_ENUM( ENUM_NAME ) enum class ENUM_NAME
#define END_ENUM ;
#include "LogCategories.m"
#undef DECL_ENUM_ELEMENT
#undef BEGIN_ENUM
#undef END_ENUM
#undef TOP
#undef BOTTOM

///// The strings associated with the enums are gererated here
/////////////////////////////////////////////////////////////////////////////////////////////
#define DECL_ENUM_ELEMENT( element ) m.insert(std::make_pair<LogCat,std::string>(LogCat::element, #element));
#define TOP std::map<LogCat,std::string> m;
#define BOTTOM return m;
#define BEGIN_ENUM( ENUM_NAME ) inline std::map<LogCat,std::string> initCats()
#define END_ENUM
#include "LogCategories.m"
#undef DECL_ENUM_ELEMENT
#undef BEGIN_ENUM
#undef END_ENUM
#undef TOP
#undef BOTTOM

///// The integers associated with the LogCat levels of enums are gererated here
/////////////////////////////////////////////////////////////////////////////////////////////
#define DECL_ENUM_ELEMENT( element ) m.insert(std::make_pair<LogCat,std::pair<unsigned int, unsigned int>>(LogCat::element, std::make_pair<unsigned int, unsigned int>(10, 10)));
#define TOP std::map<LogCat, std::pair<unsigned int, unsigned int>> m;
#define BOTTOM return m;
#define BEGIN_ENUM( ENUM_NAME ) inline std::map<LogCat,std::pair<unsigned int, unsigned int>> initCatsLevels()
#define END_ENUM
#include "LogCategories.m"
#undef DECL_ENUM_ELEMENT
#undef BEGIN_ENUM
#undef END_ENUM
#undef TOP
#undef BOTTOM


enum class LogCat;


/**
 * @brief getLogName
 * @param l
 * @return 
 */
inline std::string getLogName(LogCat l) {
	static std::map<LogCat,std::string> m = initCats();
	return m.at(l);
}

/**
 * @brief getLogMap
 * @return 
 */
inline std::map<LogCat, std::pair<unsigned int, unsigned int>>& getLogMap() {
	static std::map<LogCat,std::pair<unsigned int, unsigned int>> m = initCatsLevels();
	return m;
}

/**
 * @brief getLogLevel
 * @param l
 * @return 
 */
inline std::pair<unsigned int, unsigned int> getLogLevel(LogCat l) {
	return getLogMap().at(l);
}

} // namespace logger

#endif
