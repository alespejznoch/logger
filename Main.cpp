#include "Log.h"
#include "Settings.h"

#include <thread>

using namespace std;
using namespace logger;


std::mutex Logger::mtx;
bool Log::dev_mode;

/**
 * @brief initLog
 */
void initLog() {
	Log::getInstance().setLogToConsole(Settings::getLogToConsole());
	Log::getInstance().setLogToFile(Settings::getLogToFile());

	if (Settings::getDevMode()) {
		Log::dev_mode = true;
	} else {
		Log::dev_mode = false;
	}

	
	// remove /home/somebody/something/ from __FILE__
	std::string path = __FILE__;
	size_t pos = path.find("Main");
	Log::getInstance().setRemoveFromPath(pos);
}


/**
 * @brief Spamming - ferocious spamming of log messages
 */
void Spamming(unsigned interval)
{
	while(1)
	{

		writeLog("SPAM", LogCat::GENERIC, WARNING) << 
			"Dihydrogen monoxide is colorless, tasteless, "
			"and kills thousands of people every year.";

		this_thread::sleep_for (std::chrono::microseconds(interval));
	}
}


/**
 * @brief Spamming - different way of spamming - usefull for debug messages
 */
void Spamming2(unsigned interval)
{
	while(1)
	{

		Log::debug("Debug message!");

		this_thread::sleep_for (std::chrono::microseconds(interval));
	}
}

/**
 * @brief main
 * @return
 */
int main() {

	srand (time(NULL));

	if (Settings::getInstance().initSettings() == false)
	{
		cerr << "ERROR loading " << SETTINGS_FILE << " file" << endl;
		exit(0);
	}
	
	// pass settings.ini configuration to the Log
	initLog();

	new thread(&Spamming, 1000);
	new thread(&Spamming, 1000);
	new thread(&Spamming, 100);
	new thread(&Spamming2, 1000);


	while(1){}

	return 0;
}
