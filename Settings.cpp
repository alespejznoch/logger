#include "Settings.h"
#include "Funcs.h"

#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <regex>
#include <map>

using namespace std;
using namespace logger;

// DEFAULT VALUES

//general
bool Settings::dev_mode = true;

//logger
bool Settings::log_to_console = true;
bool Settings::log_to_file = true;

string Settings::exePath = "";

#define RED        "\033[1m\033[31m"     /* Bold Magenta */
#define RESET      "\033[0m"

/**
 * @brief Settings::initSettings
 * @return
 */
bool Settings::initSettings()
{
	const ssize_t BUF_SIZE = 256;
	char buf[BUF_SIZE];
	char pathName[32];
	sprintf(pathName, "/proc/%d/exe", getpid());
	ssize_t result = readlink(pathName, buf, BUF_SIZE);
	if (result<0 || result>BUF_SIZE-1)
	{
		return false;
	}
	buf[result] = '\0';
	
	exePath = string(buf);
	std::size_t found = exePath.find_last_of("/");
	exePath = exePath.substr(0,found+1);
	
	string settingsLocation(exePath);
	settingsLocation.append(SETTINGS_FILE);
	
	ifstream settings_file;
	settings_file.open (settingsLocation);

	if (settings_file.is_open())
	{
		string line;
		map<string,string> general, logger;
		ini_sections section = ini_sections::error;
		while (getline(settings_file,line))
		{
			//cout << line << '\n';
			line = trim(line);

			// skip empty lines and comments
			if (line.empty() || line.front()==';') {
				continue;
			}

			// set section
			regex rx("\\[(.*)\\][\\ ]*$");
			if (regex_match(line,rx)) {
				section = ini_sections::error;

				if (line.compare(1,line.size()-2,"general")==0) {
					section = ini_sections::general;
					continue;
				}

				if (line.compare(1,line.size()-2,"logger")==0) {
					section = ini_sections::logger;
					continue;
				}
			}

			if (ini_sections::error == section) {
				///@todo report error
				continue;
			}

			// fetch values
			array<string,2> item_value = splitAndTrim(line, '=');
			if (section == ini_sections::general) {
				general.insert(std::pair<string,string>(item_value[0], item_value[1]));
				continue;
			}

			if (section == ini_sections::logger) {
				logger.insert(std::pair<string,string>(item_value[0], item_value[1]));
				continue;
			}

		} // while end



		cerr << RED;
		// save variables
		// general
		if (general.count("dev_mode")!=0 && isNumber(general.at("dev_mode"))) {
			dev_mode = (bool)stoul(general.at("dev_mode"));
		} else
		{
			cerr << "ERROR: dev_mode is missing in " << SETTINGS_FILE << endl;
		}


		// logger
		map<LogCat, string> names = initCats();
		map<LogCat, pair<unsigned int, unsigned int>> &m = getLogMap();

		for (auto it=names.begin(); it!=names.end(); ++it) {
			if (logger.count(it->second)!=0) {
				vector<string> levelstr = split(logger.at(it->second), ',');
				if (isNumber(levelstr.at(0))) {
					if (levelstr.size() < 2) {
						cerr << "ERROR: Category " << it->second << " is missing 2nd value in " << SETTINGS_FILE << endl;
						levelstr.push_back(levelstr[0]);
					}
					pair<unsigned int, unsigned int> levels = pair<unsigned int, unsigned int>(stoul(levelstr[0]), stoul(levelstr[1]));
					m[it->first] = levels;
				} else
				{
					cerr << "ERROR: Category " << it->second << " is missing in " << SETTINGS_FILE << endl;
				}
			}
		}


		if (logger.count("log_to_console")!=0 && isNumber(logger.at("log_to_console"))) {
			log_to_console = (bool)stoul(logger.at("log_to_console"));
		} else
		{
			cerr << "ERROR: log_to_console is missing in " << SETTINGS_FILE << endl;
		}

		if (logger.count("log_to_file")!=0 && isNumber(logger.at("log_to_file"))) {
			log_to_file = (bool)stoul(logger.at("log_to_file"));
		} else
		{
			cerr << "ERROR: log_to_file is missing in " << SETTINGS_FILE << endl;
		}





		cerr << RESET << flush;

		settings_file.close();
		return true;
	}

	settings_file.close();
	return false;
}


string Settings::getExePath()
{
	return exePath;
}

bool Settings::getLogToFile()
{
	return log_to_file;
}

bool Settings::getLogToConsole()
{
	return log_to_console;
}

bool Settings::getDevMode()
{
	return dev_mode;
}
