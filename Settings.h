#ifndef SETTINGS_H
#define SETTINGS_H

#include <map>
#include <string>

#include "LogMagic.h"

#define SHARED_MEMORY_LOGGER
#define MEASURE_PERFORMANCE
#define SHARED_MEMORY_LOGGER
#define MEASURE_LATENCY_PERFORMANCE


namespace logger {

#define SETTINGS_FILE "settings.ini"

enum class ini_sections : unsigned int
{
	general=0,
	logger=1,
	
	error=8000,
};

/**
 * @brief The Settings class
 */
class Settings
{
public:
	/**
	 * @brief getInstance singleton
	 * @return 
	 */
	static Settings& getInstance()
	{
		static Settings settings;
		return settings;
	}
	
	/**
	 * @brief initSettings
	 * @return 
	 */
	static bool initSettings();
	

	// executable path
	static std::string exePath;

	// general
	static bool dev_mode;
	// logger
	static bool log_to_console;
	static bool log_to_file;
	

	static std::string getExePath();
	static bool getDevMode();
	static bool getLogToConsole();
	static bool getLogToFile();
};

} // namespace logger

#endif
