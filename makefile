CC = g++
CFLAGS = -g -Wall -pedantic  -pthread -I.
# CFLAGS = -O3 -Wall -pedantic  -pthread -I.
LIBS = -lglfw -ldl -pthread
OBJS = Settings.o Main.o
DEPS = $(OBJS:.o=.d)
TARGET = logger_test

%.o: %.c*
	$(CC) -c -o $@ $^ $(CFLAGS)

$(TARGET): $(OBJS)
	$(CC) -o $@ $^ $(LIBS)
	@echo done!
	rm -f ${OBJS}

.PHONY: clean

clean:
	rm -f ${OBJS} ${DEPS} $(TARGET)

